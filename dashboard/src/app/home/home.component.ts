import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
// import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {HttpClient} from "@angular/common/http";
import {MatSort, MatSortHeader, SortDirection} from '@angular/material/sort';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import { MatSelectChange } from '@angular/material/select';
export interface UserData {
  // id: string;
  firstName: string;
  email:string;
  contactNumber: number;
  age:number;
  dob:string
}


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit {
  displayedColumns: string[] = ['firstName','email','contactNumber','age','dob'];
 
  userDatabase: PhotographersDatabase | null | undefined;

  data: UserData[] = [];

  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageEvent: PageEvent | undefined;

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;


  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;
  dataSource: any;
  selectedStatus: string = '';
  
  
 

  constructor(private _httpClient: HttpClient) {}
  

  ngAfterViewInit() {
    
    
    this.userDatabase = new PhotographersDatabase(this._httpClient);
    // this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.userDatabase!.getUserDetails(
            this.sort.active,
            this.sort.direction,
            this.paginator.pageIndex,
          ).pipe(catchError(() => observableOf(null)));
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = data === null;

          if (data === null) {
            return [];
          }
    
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.sort = this.sort;
          return data;
        }),
      )
      .subscribe(data => (data = data));
      
      
  }
  
  setPageSizeOptions(setPageSizeOptionsInput: string) {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }
  }

  
  
  selectedValue(event: MatSelectChange) {
    this.selectedStatus = event.value
    const filterValue = (event).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}




/** An example database that the data source uses to retrieve data for the table. */
export class PhotographersDatabase {
  constructor(private _httpClient: HttpClient) {}
  pageLimit = 10;
  getUserDetails(sort: string, order: SortDirection, page: number): Observable<any> {
    const href = 'https://hub.dummyapis.com/employee?';
    const requestUrl = `${href}noofRecords=${this.pageLimit}&idStarts=1001`;
    return this._httpClient.get<any>(requestUrl);
    
  }
}



