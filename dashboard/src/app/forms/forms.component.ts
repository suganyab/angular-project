import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';


@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {
  allUsers: Object | any;
  isEdit = false;
  userObj ={
    name:"",
    mobile:"",
    email:"",
    password:"",
    id:""
  }

  constructor(private commonSerivce:CommonService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  addUser(formObj: any){
    console.log(formObj)
    this.commonSerivce.createUser(formObj).subscribe((response)=>{
      this.getUsers();
    })
  }
  getUsers(){
    this.commonSerivce.getAllUser().subscribe((response)=>{
      this.allUsers = response;
      console.log(this.allUsers);
    })
  }
  editUser(user: { name: string; mobile: string; email: string; password: string; id: string; }){
    this.isEdit = true;
    this.userObj =user;
  }
  
  deleteUser(user: any){
    this.commonSerivce.deleteUSer(user).subscribe((response)=>{
      this.getUsers();
    })
  }
  updateUser(){
    this.isEdit = !this.isEdit;
    this.commonSerivce.updateUser(this.userObj).subscribe(()=>{
      this.getUsers();
    });
  }
}
